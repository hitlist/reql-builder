'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('./init')
const toResponse = require('../lib/to-response')

test('to-response', async (t) => {
  const {r} = await setup()

  await t.test('singluar filter::exact', async (tt) => {
    const results = await toResponse(r, TABLE_NAME, {
      filters: {
        id: 3
      }
    })

    tt.match(results, {
      meta: {
        total: 1
      , limit: 25
      , offset: 0
      , next: null
      , previous: null
      }
    , data: [
        {id: 3}
      ]
    })
    tt.end()
  })

  await t.test('ordering', async (tt) => {
    {
      const results = await toResponse(r, TABLE_NAME, {
        filters: {
          id__in: '9,3,6'
        }
      , orderby: 'id'
      })

      tt.match(results, {
        data: [
          {id: 3}
        , {id: 6}
        , {id: 9}
        ]
      }, 'ascending order')
    }

    {
      const results = await toResponse(r, TABLE_NAME, {
        filters: {
          id__in: '9,3,6'
        }
      , orderby: '-id'
      })
      tt.match(results, {
        data: [
          {id: 9}
        , {id: 6}
        , {id: 3}
        ]
      }, 'descending order')
    }

    tt.end()
  })

  await t.test('paginated w/ ordering', async (tt) => {
    const results = await toResponse(r, TABLE_NAME, {
      orderby: '-id'
    , orderby_index: 'id'
    , limit: 3
    , offset: 3
    })

    tt.match(results, {
      data: [
        {id: 7}
      , {id: 6}
      , {id: 5}
      ]
    })
    tt.end()
  })
  await teardown()
  t.end()
})
