'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('./init')
const filter = require('../lib/filter')

test('filter::builder', async (t) => {
  const {r} = await setup()

  await t.test('singluar filter::exact', async (tt) => {
    const filters = filter(r, {id: 3})
    const results = await r.table(TABLE_NAME).filter(filters).pluck('id')
    tt.deepEqual(results, [
      {id: 3}
    ])
    tt.end()
  })

  await t.test('multiple filters', async (tt) => {
    const filters = filter(r, {
      id__in: '2,4,6,8,10'
    , email__endswith: '.biz'
    })

    const results = await r.table(TABLE_NAME).filter(filters).pluck('id')
    tt.deepEqual(results, [{id: 10}])
    tt.end()
  })

  await t.test('multiple nested filters', async (tt) => {
    const filters = filter(r, {
      address__city__icontains: 'e'
    , company__name__istartswith: 'r'
    })

    const results = await r.table(TABLE_NAME)
      .filter(filters)
      .orderBy(r.asc('id'))
      .pluck('id')

    tt.deepEqual(results, [
      {id: 1}
    , {id: 3}
    , {id: 4}
    ])
    tt.end()
  })

  await teardown()
  t.end()
})
