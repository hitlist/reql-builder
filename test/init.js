'use strict'

if (require.main === module) return

const crypto = require('crypto')
const tap = require('tap')
let thinky

const TABLE_NAME = crypto.randomBytes(5).toString('hex')
module.exports = {
  setup
, teardown
, TABLE_NAME
}

async function setup () {
  thinky = require('thinky')({db: 'test'})
  try {
    await thinky.r.tableCreate(TABLE_NAME)
  } catch (e) {
    await thinky.r.table(TABLE_NAME).delete()
  }
  const type = thinky.type
  const User = thinky.createModel(TABLE_NAME, {
    id: type.number()
  , name: type.string()
  , birthday: type.date().optional()
  , addres: type.object().schema({
      street: type.string()
    , suite: type.string()
    , city: type.string()
    , zipcode: type.string()
    , geo: type.point()
    })
  , phone: type.string()
  , website: type.string()
  , company: {
      name: type.string()
    , catchPhrase: type.string()
    , bs: type.string()
    }
  }, {
    pk: 'id'
  })

  User.ensureIndex('id')
  await User.save(require('./fixture'))
  await thinky.r.table(TABLE_NAME).wait()
  return thinky
}

async function teardown() {
  if (!thinky) return null
  await thinky.r.tableDrop(TABLE_NAME)
  await thinky.r.getPool().drain()
  return null
}
