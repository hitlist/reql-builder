'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {nin} = require('../../lib/filters')

test('filters::in', async (t) => {
  const {r} = await setup()

  await t.test('number values', async (tt) => {
    const filter = nin(r, 'id', [1, 5, 9])
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    const records = await query.map(function (i) { return i('id') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 7, 'records id > 7')
    tt.deepEqual(records, [2, 3, 4, 6, 7, 8, 10])
    tt.end()
  })

  await t.test('string values', async (tt) => {
    const names = ['Bret', 'Samantha', 'Antonette']
    const filter = nin(r, 'username', names)
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('username')).pluck('username')
    const records = await query.map(function (i) { return i('username') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 7, 'records id > 8')
    tt.deepEqual(records, [
      'Delphine'
    , 'Elwyn.Skiles'
    , 'Kamren'
    , 'Karianne'
    , 'Leopoldo_Corkery'
    , 'Maxime_Nienow'
    , 'Moriah.Stanton'
    ].sort())
    tt.end()
  })

  await t.test('nested values', async (tt) => {
    const cities = ['Howemouth', 'Lebsackbury', 'Fake City']
    const filter = nin(r, 'address__city', cities)
    const query = r.table(TABLE_NAME).filter(filter).pluck({address: {city: true}})
    const records = await query.map(function (i) { return i('address')('city') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 8, 'records id > 8')
    tt.deepEqual(records.sort(), [
      'Aliyaview'
    , 'Bartholomebury'
    , 'Gwenborough'
    , 'McKenziehaven'
    , 'Roscoeview'
    , 'South Christy'
    , 'South Elvis'
    , 'Wisokyburgh'
    ].sort())
    tt.end()
  })

  await teardown()
  t.end()
})
