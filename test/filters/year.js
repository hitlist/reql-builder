'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {year} = require('../../lib/filters')

test('filters::year', async (t) => {
  const {r} = await setup()
  await t.test('string values `2017`', async (tt) => {
    const filter = year(r, 'birthday', '2017')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.deepEqual(records, [
      {id :9}
    ])
    tt.end()
  })

  await t.test('number values', async (tt) => {
    const filter = year(r, 'birthday', 2018)
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 2, 'records count = 2')
    tt.deepEqual(records, [{id: 10}, {id: 1} ])
    tt.end()
  })

  await teardown()
  t.end()
})
