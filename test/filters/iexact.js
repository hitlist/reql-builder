'use strict'

const {test} = require('tap')
const {setup, teardown,TABLE_NAME} = require('../init')
const {iexact} = require('../../lib/filters')

function _map(item) {
  return item('id')
}

test('filters::iexact', async (t) => {
  const {r} = await setup()

  await t.test('username iexact BRET', async(tt) => {
    const filter = iexact(r, 'username', 'BRET')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.deepEqual(records, [{id: 1}])
    tt.end()
  })
  await t.test('company__name iexact YOST and sOns', async (tt) => {
    const filter = iexact(r, 'company__name', 'YOST and sOns')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.deepEqual(records, [{id: 9}])
    tt.end()
  })

  await teardown()
  t.end()
})
