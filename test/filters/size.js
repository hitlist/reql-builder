'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {size} = require('../../lib/filters')

function _map(item) {
  return item('id')
}

test('filters::size', async (t) => {
  const {r} = await setup()
  await t.test('string values', async (tt) => {
    const filter = size(r, 'tags', '3')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 2, 'records count = 2')
    tt.deepEqual(records, [
      {id :1}
    , {id: 6}
    ])
    tt.end()
  })

  await t.test('number values', async (tt) => {
    const filter = size(r, 'tags', 2)
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.deepEqual(records, [{id :10}])
    tt.end()
  })

  await teardown()
  t.end()
})
