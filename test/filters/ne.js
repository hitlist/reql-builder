'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {ne} = require('../../lib/filters')

function _map(item) {
  return item('username')
}

test('filters::ne', async (t) => {
  const {r} = await setup()

  await t.test('number values', async(tt) => {
    const filter = ne(r, 'id', 5)
    const records = await r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 9, 'records count = 9')
    tt.deepEqual(records, [
      {id: 1}
    , {id: 2}
    , {id: 3}
    , {id: 4}
    , {id: 6}
    , {id: 7}
    , {id: 8}
    , {id: 9}
    , {id: 10}
    ])
    tt.end()
  })

  await t.test('multiple number values', async(tt) => {
    let filter = ne(r, 'id', 5)
    filter = filter.and(ne(r, 'id', 9))
    const records = await r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 8, 'records count = 8')
    tt.deepEqual(records, [
      {id: 1}
    , {id: 2}
    , {id: 3}
    , {id: 4}
    , {id: 6}
    , {id: 7}
    , {id: 8}
    , {id: 10}
    ])
    tt.end()
  })

  await t.test('string values', async (tt) => {
    const filter = ne(r, 'username', 'Bret')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('username').map(_map)
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 9, 'records count = 9')
    tt.deepEqual(
      records.sort()
    , [
        'Karianne','Kamren','Samantha','Antonette', 'Leopoldo_Corkery'
      , 'Moriah.Stanton','Delphine','Elwyn.Skiles','Maxime_Nienow'
      ].sort()
    )
    tt.end()
  })

  await t.test('nested values', async (tt) => {
    const filter = ne(r, 'company__name', 'Deckow-Crist')
    const records = await r.table(TABLE_NAME)
      .filter(filter)
      .map(function(i) {
        return i('company')('name')
      })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 9, 'records count = 9')
    tt.match(records.sort(), [
      'Robel-Corkery','Keebler LLC','Romaguera-Jacobson','Romaguera-Crona'
    , 'Considine-Lockman','Hoeger LLC','Yost and Sons','Johns Group','Abernathy Group'
    ].sort())
    tt.end()
  })

  await teardown()
  t.end()
})
