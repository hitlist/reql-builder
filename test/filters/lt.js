'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {lt} = require('../../lib/filters')

test('filters::gt', async (t) => {
  const {r} = await setup()
  const filter = lt(r, 'id', 5)
  const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
  const records = await query.map(function (i) { return i('id') })
  t.type(records, Array, 'returns an array')
  t.equal(records.length, 4, 'records id < 4')
  t.deepEqual(records, [1, 2, 3, 4])
  await teardown()
  t.end()
})
