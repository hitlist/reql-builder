'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {gt} = require('../../lib/filters')

test('filters::gt', async (t) => {
  const {r} = await setup()
  const filter = gt(r, 'id', 5)
  const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
  const records = await query.map(function (i) { return i('id') })
  t.type(records, Array, 'returns an array')
  t.equal(records.length, 5, 'records id > 5')
  t.deepEqual(records, [6, 7, 8, 9, 10])
  await teardown()
  t.end()
})
