'use strict'
const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {range} = require('../../lib/filters')

test('filters::range', async (t) => {
  const {r} = await setup()
  await t.test('valid date range', async (tt) => {
    const filter = range(r, 'birthday', '2018-01-09,2018-01-11')
    const records = await r.table(TABLE_NAME).filter(filter)
    tt.type(records, Array)
    tt.equal(records.length, 1)
    tt.end()
  })
  await t.test('invalid date range', (tt) => {
    tt.throws(() => {
      const filter = range(r, 'birthday', '2018-02-09,2018-01-11')
    })
    tt.end()
  })
  await teardown()
  t.end()
})
