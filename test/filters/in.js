'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const infilter = require('../../lib/filters').in

test('filters::in', async (t) => {
  const {r} = await setup()

  await t.test('number values', async (tt) => {
    const filter = infilter(r, 'id', [1, 5, 9])
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    const records = await query.map(function (i) { return i('id') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 3, 'records id > 3')
    tt.deepEqual(records, [1, 5, 9])
    tt.end()
  })

  await t.test('string values', async (tt) => {
    const names = ['Bret', 'Samantha', 'Antonette']
    const filter = infilter(r, 'username', names)
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('username')).pluck('username')
    const records = await query.map(function (i) { return i('username') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 3, 'records id > 3')
    tt.deepEqual(records, names.sort())
    tt.end()
  })

  await t.test('nested values', async (tt) => {
    const cities = ['Howemouth', 'Lebsackbury', 'Fake City']
    const filter = infilter(r, 'address__city', cities)
    const query = r.table(TABLE_NAME).filter(filter).pluck({address: {city: true}})
    const records = await query.map(function (i) { return i('address')('city') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 2, 'records id > 2')
    tt.deepEqual(records.sort(), ['Howemouth', 'Lebsackbury'])
    tt.end()
  })

  await teardown()
  t.end()
})
