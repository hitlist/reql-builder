'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {gte} = require('../../lib/filters')

test('filters::gte', async (t) => {
  const {r} = await setup()
  const filter = gte(r, 'id', 5)
  const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
  const records = await query.map(function (i) { return i('id') })
  t.type(records, Array, 'returns an array')
  t.equal(records.length, 6, 'records id >= 6')
  const expected = [5, 6, 7, 8, 9, 10]
  t.deepEqual(records, expected, 'expected ids')
  await teardown()
  t.end()
})

