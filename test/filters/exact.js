'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {exact} = require('../../lib/filters')

function _map(item) {
  return item('id')
}

test('filters::exact', async (t) => {
  const {r} = await setup()

  await t.test('number values', async(tt) => {
    const filter = exact(r, 'id', 5)
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.deepEqual(records, [{id: 5}])
    tt.end()
  })
  await t.test('string values', async (tt) => {
    const filter = exact(r, 'username', 'Bret')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('username')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.deepEqual(records, [{username: 'Bret'}])
    tt.end()
  })

  await t.test('nested values', async (tt) => {
    const filter = exact(r, 'company__name', 'Deckow-Crist')
    const records = await r.table(TABLE_NAME).filter(filter)
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.match(records, [
      {id: 2}
    ])
    tt.end()
  })

  await teardown()
  t.end()
})
