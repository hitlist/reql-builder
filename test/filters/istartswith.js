'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {istartswith} = require('../../lib/filters')

test('filters::istartswith', async (t) => {
  const {r} = await setup()
  await t.test('username istartswith a', async (tt) => {
    const filter = istartswith(r, 'username', 'a')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('username')).pluck('username')
    const records = await query.map(function (i) { return i('username') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id = 1')
    tt.deepEqual(records, ['Antonette'])
  })

  await t.test('company__name istartswith DE', async (tt) => {
    const filter = istartswith(r, 'company__name', 'DE')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id'))
    const records = await query.map(function (i) { return i('company')('name') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id = 1')
    tt.deepEqual(records, ['Deckow-Crist'])
  })

  await teardown()
  t.end()
})
