'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {all} = require('../../lib/filters')

test('filters::all', async (t) => {
  const {r} = await setup()

  await t.test('tags contains [one, two]', async (tt) => {
    const filter = all(r, 'tags', ['one', 'two'])
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    const records = await query.map(function (i) { return i('id') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 3, 'records id length =  3')
    tt.deepEqual(records, [1, 6, 10])
    tt.end()
  })

  await t.test('tags contains [one, two, three]', async (tt) => {
    const filter = all(r, 'tags', ['one', 'two', 'three'])
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    const records = await query.map(function (i) { return i('id') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 2, 'records id length = 2')
    tt.deepEqual(records, [1, 6])
    tt.end()
  })

  await t.test('tags contains [one, two, three, four]', async (tt) => {
    const filter = all(r, 'tags', ['one', 'two', 'three', 'four'])
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    const records = await query.map(function (i) { return i('id') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 0, 'records id length = 0')
    tt.deepEqual(records, [])
    tt.end()
  })
  await teardown()
  t.end()
})
