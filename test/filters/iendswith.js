'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {iendswith} = require('../../lib/filters')

test('filters::iendswith', async (t) => {
  const {r} = await setup()
  await t.test('username iendswith et', async (tt) => {
    const filter = iendswith(r, 'username', 'et')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('username')).pluck('username')
    const records = await query.map(function (i) { return i('username') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id > 1')
    tt.deepEqual(records, ['Bret'])
    tt.end()
  })

  await t.test('company__name iendswith IST', async (tt) => {
    const filter = iendswith(r, 'company__name', 'ist')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id'))
    const records = await query.map(function (i) { return i('company')('name') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id > 1')
    tt.deepEqual(records, ['Deckow-Crist'])
    tt.end()
  })

  await t.test('company__name iendswith IST', async (tt) => {
    const filter = iendswith(r, 'company__name', 'IST')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id'))
    const records = await query.map(function (i) { return i('company')('name') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id > 1')
    tt.deepEqual(records, ['Deckow-Crist'])
    tt.end()
  })
  await teardown()
  t.end()
})
