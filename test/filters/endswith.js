'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {endswith} = require('../../lib/filters')

test('filters::endswith', async (t) => {
  const {r} = await setup()
  await t.test('username endswith et', async (tt) => {
    const filter = endswith(r, 'username', 'et')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('username')).pluck('username')
    const records = await query.map(function (i) { return i('username') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id > 1')
    tt.deepEqual(records, ['Bret'])
  })

  await t.test('company__name endswith IST', async (tt) => {
    const filter = endswith(r, 'company__name', 'ist')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id'))
    const records = await query.map(function (i) { return i('company')('name') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id > 1')
    tt.deepEqual(records, ['Deckow-Crist'])
  })

  await t.test('company__name endswith IST', async (tt) => {
    const filter = endswith(r, 'company__name', 'IST')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id'))
    const records = await query.map(function (i) { return i('company')('name') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 0, 'records id > 0')
    tt.deepEqual(records, [])
  })
  await teardown()
  t.end()
})
