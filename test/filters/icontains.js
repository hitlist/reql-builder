'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {icontains} = require('../../lib/filters')

function _map(item) {
  return item('email')
}

test('filters::icontains', async (t) => {
  const {r} = await setup()

  await t.test('number values', async(tt) => {
    const filter = icontains(r, 'email', '.BiZ')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('email').map(_map)
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 3, 'records count = 3')
    tt.deepEqual(records, [
      'Rey.Padberg@karina.biz'
    , 'Sincere@april.biz'
    , 'Telly.Hoeger@billy.biz'
    ])
    tt.end()
  })
  await t.test('email contains is case insensitive', async(tt) => {
    const filter = icontains(r, 'email', '.bIz')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('email').map(_map)
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 3, 'records count = 3')
    tt.deepEqual(records, [
      'Rey.Padberg@karina.biz'
    , 'Sincere@april.biz'
    , 'Telly.Hoeger@billy.biz'
    ])
    tt.end()
  })

  await teardown()
  t.end()
})
