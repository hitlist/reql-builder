'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {startswith} = require('../../lib/filters')

test('filters::startswith', async (t) => {
  const {r} = await setup()
  await t.test('username startswith A', async (tt) => {
    const filter = startswith(r, 'username', 'A')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('username')).pluck('username')
    const records = await query.map(function (i) { return i('username') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id = 1')
    tt.deepEqual(records, ['Antonette'])
  })

  await t.test('company__name startswith De', async (tt) => {
    const filter = startswith(r, 'company__name', 'De')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id'))
    const records = await query.map(function (i) { return i('company')('name') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records id = 1')
    tt.deepEqual(records, ['Deckow-Crist'])
  })

  await t.test('company__name startswith DE', async (tt) => {
    const filter = startswith(r, 'company__name', 'DE')
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id'))
    const records = await query.map(function (i) { return i('company')('name') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 0, 'records id = 0')
    tt.deepEqual(records, [])
  })
  await teardown()
  t.end()
})
