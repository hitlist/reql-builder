'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {month} = require('../../lib/filters')

test('filters::month', async (t) => {
  const {r} = await setup()
  await t.test('string values - january', async (tt) => {
    const filter = month(r, 'birthday', 'january')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.deepEqual(records, [
      {id :1}
    ])
    tt.end()
  })

  await t.test('string values - february', async (tt) => {
    const filter = month(r, 'birthday', 'february')
    const records = await r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 2, 'records count = 2')
    tt.deepEqual(records, [
      {id :9}
    , {id :10}
    ])
    tt.end()
  })

  await t.test('invalid month', async (tt) => {
    tt.throws(() => {
      month(r, 'birthday', 'fake')
    })
    tt.end()
  })

  await teardown()
  t.end()
})
