'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {day} = require('../../lib/filters')

test('filters::day', async (t) => {
  const {r} = await setup()
  await t.test('wednesday', async (tt) => {
    const filter = day(r, 'birthday', 'wednesday')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 2, 'records count = 2')
    tt.deepEqual(records, [
      {id :9}
    , {id: 1}
    ])
    tt.end()
  })

  await t.test('saturday', async (tt) => {
    const filter = day(r, 'birthday', 'saturday')
    const records = await r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count = 1')
    tt.deepEqual(records, [
      {id :10}
    ])
    tt.end()
  })

  await t.test('invalid day', async (tt) => {
    tt.throws(() => {
      day(r, 'birthday', 'fake')
    })
    tt.end()
  })

  await teardown()
  t.end()
})
