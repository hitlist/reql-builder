'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {isnull} = require('../../lib/filters')

test('filters::isnull', async (t) => {
  const {r} = await setup()
  await t.test('isnull=true', async(tt) => {
    const filter = isnull(r, 'website', true)
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    const records = await query.map(function (i) { return i('id') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 2, 'records count 2')
    tt.deepEqual(records, [2, 8])
    tt.end()
  })

  await t.test('isnull=0', async (tt) => {
    const filter = isnull(r, 'website', 0)
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    const records = await query.map(function (i) { return i('id') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 8, 'records count 8')
    tt.deepEqual(records, [1, 3, 4, 5, 6, 7, 9, 10])
    tt.end()
  })

  await t.test('nested isnull=true', async (tt) => {
    const filter = isnull(r, 'address__geo', true)
    const query = r.table(TABLE_NAME).filter(filter).orderBy(r.asc('id')).pluck('id')
    const records = await query.map(function (i) { return i('id') })
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 1, 'records count 1')
    tt.deepEqual(records, [10])
    tt.end()
  })
  await teardown()
  t.end()
})
