'use strict'

const {test} = require('tap')
const {setup, teardown, TABLE_NAME} = require('../init')
const {contains} = require('../../lib/filters')

function _map(item) {
  return item('email')
}

test('filters::contains', async (t) => {
  const {r} = await setup()

  await t.test('email contains .biz', async(tt) => {
    const filter = contains(r, 'email', '.biz')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('email').map(_map)
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 3, 'records count = 3')
    tt.deepEqual(records, [
      'Rey.Padberg@karina.biz'
    , 'Sincere@april.biz'
    , 'Telly.Hoeger@billy.biz'
    ])
    tt.end()
  })

  await t.test('email contains is case sensitive', async(tt) => {
    const filter = contains(r, 'email', '.bIz')
    const records = await r.table(TABLE_NAME).filter(filter).pluck('email').map(_map)
    tt.type(records, Array, 'returns an array')
    tt.equal(records.length, 0, 'records count = 0')
    tt.deepEqual(records, [])
    tt.end()
  })

  await teardown()
  t.end()
})
