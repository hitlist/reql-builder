'use strict'

module.exports = function typecast(val) {
  if ( val === null || val === 'null' ) return null
  if ( val === 'true' ) return true
  if ( val === 'false' ) return false
  if ( val === undefined || val === 'undefined' ) return undefined
  if ( val === '' || isNaN(val) ) return val
  return parseFloat(val);
}
