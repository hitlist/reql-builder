'use strict'

module.exports = function toArray(item) {
  if (item == null) return []
  if (Array.isArray(item)) return item.slice()
  return typeof item === 'string' ? item.split(',') : [item]
}
