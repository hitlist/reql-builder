'use strict'
const SEP = '__'

module.exports = function toField( r, field ){
  const bits = field.split(SEP)
  let ret = r.row(bits.shift())

  while (bits.length) {
    ret = ret( bits.shift())
  }
  return ret
}
