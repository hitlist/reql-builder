'use strict'

const toArray = require('./lang/to-array')
const ORDER_EXP = /^(\-)?([\w]+)/
const SEP = '__'

module.exports = sort

function sort(r, query, orderby = []) {
  for (const param of toArray(orderby)) {
    const {
      index = null
    , field = null
    } = param
    if (!field) continue

    const bits = ORDER_EXP.exec(field)
    if (!bits || !bits.length) continue

    const dir = bits[1] ? 'desc' : 'asc'
    const fn = r[dir]

    query = index
      ? query.orderBy({index: fn(index)})
      : query.orderBy(fn(bits[2]))
  }
  return query
}
