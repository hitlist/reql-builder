/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * Filters documents where an array field contains exactly the specified number of elements
 * @module reql-builder/lib/filters/size
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__size=10
 */

const toField = require('../to-field')

module.exports = function size(r, field, term){
  return toField(r, field).count().eq(+term)
}
