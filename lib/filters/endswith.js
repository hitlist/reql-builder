/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * Filter comparing a value to the end of a string
 * @module reql-builder/lib/filters/endswith
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/toField
 * @example field__endswith=bar
 */

const toField = require('../to-field')

module.exports = function endswith(r, field, term) {
  return toField(r, field).match(term + '$')
}
