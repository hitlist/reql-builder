/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters for value that match the provided string at any location in a case insensitive fashion
 * @module reql-builder/lib/filters/icontains
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__icontains=HELLO
 */

const toField = require('../to-field')

module.exports = function icontains(r, field, term) {
  return toField(r, field).match('(?i)' + term)
}
