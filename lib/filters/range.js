/*jshint laxcomma: true, smarttabs: true, node:true, mocha: true*/
'use strict'
/**
 * Restricts a date value between a finite boundry
 * @module reql-builder/lib/filters/range
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @requires reql-builder/lib/lang/to-array
 * @example ?foobar__range=2015-04-01,2015-07-01
 */
const toField = require('../to-field')
const toArray = require('../lang/to-array')
const DATE_EXP = /^\d{4}\-\d{2}\-\d{2}$/

module.exports = function range(r, field, term){
  const terms = toArray(term)
  const lower = _validate(terms[0])
  const upper = _validate(terms[1])
  if( lower > upper ){
    const error = new RangeError('Dates out of range')
    error.code = 'ERR_DATERANGE'
    throw error
  }

  return toField( r, field)
  .during(
    r.time(
      lower.getFullYear()
    , lower.getMonth() + 1
    , lower.getDate()
    , 'Z'
    )
  , r.time(
      upper.getFullYear()
    , upper.getMonth() + 1
    , upper.getDate() + 1
    , 'Z'
    )
  )
}

function _validate(date) {
  if (!DATE_EXP.test) {
    const error = new Error('dates must be in YYYY-MM-DD formt')
    error.code = 'ERR_DATEFORMAT'
    throw error
  }
  return new Date(date)
}
