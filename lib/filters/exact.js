/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * Filter comparing a value exactly
 * @module reql-builder/lib/filters/exact
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__exact=bar
 */

const toField = require('../to-field')

module.exports = function exact(r, field, term)  {
  return toField(r, field).eq(term)
}
