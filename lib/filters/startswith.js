/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters documents where the field value matches the beginning of the document value
 * @module reql-builder/lib/filters/size
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__size=10
 */

const toField = require('../to-field')

module.exports = function size(r, field, term) {
  return toField(r, field).match("^" + term)
}
