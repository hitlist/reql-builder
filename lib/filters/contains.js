/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict';
/**
 * filters for value that match the provided string at any location
 * @module reql-builder/lib/filters/contains
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires tastypie-rethink/lib/toField
 * @example field__contains=foo
 * @example field__contains=bar
 * @example field__contains=oo
 * @example field__contains=ar
 */

const toField = require('../to-field')

module.exports = function contains(r, field, term) {
  return toField(r, field).match(term)
}
