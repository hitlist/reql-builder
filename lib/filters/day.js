/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters documents where the field value matches the beginning of the document value
 * @module reql-builder/lib/filters/day
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires tastypie-rethink/lib/toField
 * @example field__day=monday
 * @example field__day=tuesday
 */

const toField = require('../to-field')
const days = ['monday', 'tuesday', 'wednesday','thursday','friday','saturday','sunday']

module.exports = function day( r, field, term ){
  if (!days.includes(term)) {
    const error = new Error(`${field} must be one of ${days.join}`)
    throw error
  }
  return toField(r, field).dayOfWeek( ).eq( r[term] )
}
