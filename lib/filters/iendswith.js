/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * Filter comparing a value to the end of a string in a case insensitive fashion
 * @module reql-builder/lib/filters/iendswith
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__iendswith=ing
 */

const toField = require('../to-field')

module.exports = function iendswith(r, field, term) {
  return toField(r, field ).match('(?i)' + term + '$')
}
