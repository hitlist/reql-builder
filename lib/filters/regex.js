/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters documents where the document field matches a specified regex
 * @module reql-builder/lib/filters/regex
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__regex=^A
 * @example field__regex=^b$
 * @example field__regex=(?i)^jo
 */

const toField = require('../to-field')

module.exports = function regex( r, field, term ){
  return toField(r, term).match(decodeURIComponent(term))
}
