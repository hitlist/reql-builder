/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict';
/**
 *
 * @module reql-builder/lib/filters/all
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/toField
 * @requires reql-builder/lib/lang/to-array
 */

const toField = require('../to-field')
const toArray = require('../lang/to-array')

module.exports = function( r, field, term ){
  const terms = toArray(term)
  let filter = toField(r, field).contains(terms.shift())
  while( terms.length ){
    filter = filter.and(toField(r, field).contains(terms.shift()))
  }
  return filter
}
