/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * Filters documents were the document field value is less than the specified value
 * @module reql-builder/lib/filters/lt
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/toField
 * @example field__lt=10
 */

const toField = require('../to-field')

module.exports = function lt( r, field, term ){
  return toField(r, field ).lt( term )
}
