/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * Filters documents where the document field value is not equal to the specified Value
 * @module reql-builder/lib/filters/ne
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__ne=10
 */

const toField = require('../to-field')
const typecast = require('../lang/typecast')

module.exports = function ne(r, field, term) {
  return toField(r, field).ne(typecast(term))
}
