/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters for documents where a field contains a value greater than the value provided
 * @module reql-builder/lib/resource/filters/gt
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/toField
 * @example field__gt=1
 */

const toField = require('../to-field')

module.exports = function gt(r, field, term) {
  return toField(r, field).gt(term)
};
