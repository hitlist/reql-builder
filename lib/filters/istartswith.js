/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters documents where the field value matches the beginning of the document value in a case insensitive fashion
 * @module reql-builder/lib/filters/istartswith
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/toField
 * @example field__istartswith=foo
 */

const toField = require('../to-field')

module.exports = function istartswith(r, field, term) {
  return toField(r, field ).match('(?i)^' + term)
}
