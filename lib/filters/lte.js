/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * Filters documents were the document field value is less than or equal to the specified value
 * @module reql-builder/lib/filters/lte
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__lte=10
 */

const toField = require('../to-field')

module.exports = function lte( r, field, term ){
  return toField(r, field ).le( term )
}
