/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters for documents where a field contains a value greater than or equal to the value provided
 * @module reql-builder/lib/filters/gte
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__gte=1
 */

const toField = require('../to-field')

module.exports = function gte(r, field, term) {
  return toField(r, field).ge(term)
}
