/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters for documents where a field contains a value greater than the value provided
 * @module reql-builder/lib/filters/gt
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/toField
 * @example field__isnull=true
 */

const toField = require('../to-field')
const typecast = require('../lang/typecast')

module.exports = function gt(r, field, term) {
  term = !!(typeof term === 'string' ? typecast(term) : term)
  let query = toField(r, field).eq(null)
  return (term ? query : query.not())
}
