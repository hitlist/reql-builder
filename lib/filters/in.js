/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters documents where an array field contains one or more of the specified values
 * @module reql-builder/lib/resource/filters/in
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/filters/in
 * @requires mout/lang/isString
 * @requires mout/lang/toArray
 * @example field__in=foo
 * @example field__in=foo,bar
 * @example field__in=foo&field__in=bar
 */

const toField = require('../to-field')
const typecast = require('../lang/typecast')
const toArray = require('../lang/to-array')

module.exports = function infilter(r, field, term) {
  const terms = toArray(term)
  var filter = toField(r, field).eq(typecast(terms.shift()))
  while( terms.length ){
    filter = filter.or(toField(r, field).eq(typecast(terms.shift())))
  }
  return filter
}
