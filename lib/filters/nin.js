/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters documents where an array field does not contain any of the specified values
 * @module reql-builder/lib/filters/nin
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__nin=10
 * @example field__nin=foo,bar
 * @example field__nin=foo&field__nin=bar
 */

const infilter = require('./in')

module.exports = function nin(r, field, term) {
  return infilter(r, field, term).not()
}
