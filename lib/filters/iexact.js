/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * Filter comparing a value exactly in a case insensitive fashion
 * @module reql-builder/lib/filters/iexact
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/to-field
 * @example field__iexact=FoOBar
 */

const toField = require('../to-field')

module.exports = function iexact(r, field, term) {
  return toField(r, field ).match( '(?i)' + term)
}
