/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict'
/**
 * filters documents where the field value matches the beginning of the document value
 * @module reql-builder/lib/filters/month
 * @author Eric Satterwhite
 * @since 1.0.0
 * @requires reql-builder/lib/toField
 * @example field__month=march
 * @example field__month=december
 */

const toField = require('../to-field')
const months = [
'january', 'february', 'march', 'april'
, 'may', 'june', 'july', 'august', 'september'
, 'october', 'november', 'december'
]

module.exports = function month(r, field, term) {
  if(!months.includes(term)) {
    const error = new Error(`${field} must be one of ${months.join()}\ngot ${term}`)
    throw error
  }
  return toField(r, field).month().eq(r[term])
}
