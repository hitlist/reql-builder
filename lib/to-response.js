'use strict'

/**
 * Parses a hemera request object into a
 * @module reql-builder/lib/toRequest
 * @author Eric Satterwhite
 * @since 1.0.0
 **/

const filter = require('./filter')
const sort = require('./sort')
const ORDER_EXP = /^(\-)?([\w]+)/
const SEP = '__'
module.exports = toResponse

function toResponse(r, table, options = {}) {
  const {
    limit = 25
  , offset = 0
  , orderby = null
  , orderby_index = false
  , filters = {}
  } = options

  return r.object(
    'meta'
  , r.object(
      'total'
    , r.table(table).filter(filter(r, filters)).count()
    , 'limit'
    , limit
    , 'offset'
    , offset
    , 'next'
    , null
    , 'previous'
    , null
    )
  , 'data'
  , sort(r, r.table(table), {
      field: orderby
    , index: orderby_index
    })
    .filter(filter(r, filters))
    .skip(offset)
    .limit(limit)
    .coerceTo('array')
  )
}
