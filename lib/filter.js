'use strict'

const terms = require('./filters')
const typecast = require('./lang/typecast')
const SEP = '__'
const EMPTY_ARRAY = []

module.exports = filter

function filter(r, filters = {}) {
  let remaining = null

  for( var key in filters ){
    let bits = key.split(  SEP  )
     , filtername = 'exact'
     , filtertype = terms.exact
     , current
     , filter
     , fieldname
     , attr
     ;

    fieldname  = bits.shift();
    if (terms[bits[bits.length -1]]) {
      filtertype = terms[bits.pop()]
    }
    fieldname = bits.unshift(fieldname) && bits.join(SEP)
    filter = filtertype(r, fieldname, typecast(filters[key]))
    remaining = remaining ? remaining.and(filter) : filter
  }
  return remaining || {}
}
