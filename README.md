# reql-builder
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/322bc6c1eb0c4ec790fbed6d01d580af)](https://www.codacy.com/app/esatterwhite/reql-builder?utm_source=hitlist@bitbucket.org&amp;utm_medium=referral&amp;utm_content=hitlist/reql-builder&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/322bc6c1eb0c4ec790fbed6d01d580af)](https://www.codacy.com/app/esatterwhite/reql-builder?utm_source=hitlist@bitbucket.org&amp;utm_medium=referral&amp;utm_content=hitlist/reql-builder&amp;utm_campaign=Badge_Coverage)

Dynamic filter builder for [rethinkdbdash](https://github.com/neumino/rethinkdbdash) inspired by https://github.com/node-tastypie/tastypie-rethink

### Paging
You can use a number of special query string params to control how data is paged on the list endpoint. Namely -

* `limit` - Page size ( default 25 )
* `offset` - The starting point in the list

`limit=25&offset=50` would be the start of page 3

### Sorting
sorting is handled query param orderby where you can pass it the name of a field to sort on. Sorting is descending by default. Specifying a negetive field ( -<FOO> ) would flip the order

### Advanced Filtering
You might have noticed the filtering field on the schema. One of the things that makes an API "Good" is the ability to use query and filter the data to get very specific subsets of data. Tastypie exposes this through the query string as field and filter combinations. By default, the resource doesn't have anything enabled, you need to specify which filters are allowed on which fields, or specify 1 to allow everything

#### Filter Types

| Filter      | function                                         |
| ------------|------------------------------------------------- |
| gt          | greater than                                     |
| gte         | greater than or equal to                         |
| lt          | less than                                        |
| lte         | less than or equal to                            |
| in          | Value in set ( [ 1,2,3 ])                        |
| nin         | Value Not in set                                 |
| size        | Size of set ( array length )                     |
| startswith  | Case Sensitive string match                      |
| istartswith | Case Insensitive string match                    |
| endswith    | Case Sensitive string match                      |
| iendswith   | Case Insensitive string match                    |
| contains    | Case Sensitive global string match               |
| icontains   | Case Insensitive global string match             |
| exact ( = ) | Exact Case Sensitive string match                |
| iexact      | Exact Case Insensitive string match              |
| isnull      | matches null values                              |
| month       | Matches date values on a specific month          |
| day         | Matches date values on a speciec day of the week |
| year        | Matches date values on a specific year           |
| all         | Matches all elements in an array                 |
| range       | Matches dates withing a specified range          |
| ne          | not equal to                                     |

Filters are added by appending a double underscore ``__`` and the filter type to the end of a field name. Given our example, if we wanted to find people who were older than 25, we would use the following URI syntax

```bash
http://localhost:3000/api/v1/user?age__gt=25
```
Filters are additive for a given field. For example, if we we only wanted people where we between 25 and 45, we would just add another filter

```bash
http://localhost:3000/api/v1/user?age__gt=25&age__lt=45
```

The same double underscore `__` syntax is used to access nested fields where the filter is always the last parameter. So we could find people whos age was  **greater than** 25, **less than** 45 and whose Company Name **starts with** `W`

```bash
http://localhost:3000/api/v1/user?age__gt=25&age__lt=45&company__name__istartswith=w
```

expressions with no filters will use the default `exact` filter

```bash
http://localhost:3000/api/v1/user?age=44
```

## API

### filter(r `<rethinkdbdash>`, filters `<Object>`): `Query`

* `r` - a rethinkdb dash connection w/ a default database configured
* `filters` - an object to generate a query from. Typically a result from querystring.parse
* **returns** a rethink filter query suitable to pass to `r.filter`


Given a table of user records

```javascript
{
  "email": "billybob@bobsfarms.biz"
, name: {
    "first": "Billy"
  , "last": "Bob"
  }
}
```

```javascript
const qs = require('querystring')
const {filter} = require('@allstar/reql-builder')
const r = require('rethinkdbdash')({db: 'test'})

// emails w/ .biz
// name.first = ^bi

const filters = filter(r, {
  email__icontains: '.biz'
, name__first__istartswith: 'bi'
})

// same as

const opts = qs.parse('email__contains=.biz&name__first__istartswith=bi')
const filters = filter(r, opts)


r.table('users').filter(filters).then((results) => {
  console.log(results)
})
```

### toResponse(r `<rethinkdbdash>`, table `<String>` [,options] `<Object>`): Promise

returns a paginated list of records suitable for an api response directly from the database

#### options

- **limit** `<number>` [25] the maximum number of records to return
- **offset** `<number>` [0] The number of records to skip over
- **filters** `[object] Filters to apply the the dataset before pagination
- **orderby** `[string]` The filed and direction (`-/+`) to sort the records by
- **orderby_index** `[string]` the name of the index to use to sort by - it should include the field

* With out an index to order by performance can degrade very quickly with large datasets

```javascript
const toResponse = require('@allstar/reql-builder')
const r = require('rethinkdbdash')({db: 'test'})

toResponse(r, 'users', {
  orderby: '-id'
, orderby_index: 'user_id_idx'
, limit: 5
, offset: 5 // page 2
, filters: {email__iendswith: '.biz'}
})
.then(console.log)

{
  "meta": {
    total: 7
  , limit: 5
  , offset: 5
  , next: null
  , previous: null
  }
, "data" :[
    {id: 6, email: 'fake-6@test.biz'}
  , {id: 7, email: 'fake-7@test.biz'}
  ]
}
```
